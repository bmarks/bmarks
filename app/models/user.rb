require 'digest'

class User < ActiveRecord::Base
  attr_accessor :password
  attr_accessible :name, :email, :password, :password_confirmation

  has_many :bookmarks

  validates_presence_of :name, :email, :password
  validates_length_of :name, :maximum => 40
  validates_length_of :password, :minimum => 6
  validates_uniqueness_of :email
  validates_format_of :email, :with => /\A[\w+\-.]+@[\w\d\-.]+\.\w+\z/i
  validates_confirmation_of :password

  before_save :encrypt_password

  def self.authenticate(email, passwd)
    user = find_by_email(email)
    if not user.nil?
      if user.check_password?(passwd)
        puts "user check password passed"
        return user
      end
    end
  end

  def check_password?(p)
    self.encrypted_password == Digest::SHA2.hexdigest("#{self.salt}#{p}")
  end

  def reset_login_token!
    self.login_token = Digest::SHA2.hexdigest("#{Time.now.utc}#{self.salt}#{self.id}")
    self.save_without_validation
  end

  private
    def encrypt_password
      if not self.password.nil?
        now = Time.now.utc
        self.salt = Digest::SHA2.hexdigest("#{now}#{self.password}")
        self.encrypted_password = Digest::SHA2.hexdigest("#{self.salt}#{self.password}")
      end
    end
end
