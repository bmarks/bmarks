class Bookmark < ActiveRecord::Base
  attr_accessible :title, :link, :user_id

  belongs_to :user

  validates_format_of :link, :with => /:\/\//
  validates_presence_of :title, :link, :user_id
end
