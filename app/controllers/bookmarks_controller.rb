class BookmarksController < ApplicationController
  def index
    @title = "All bookmarks"
    @bookmarks = Bookmark.all(:conditions => ["user_id = ?", self.current_user.id])
  end

  def new
    @title = "Add bookmark"
  end

  def create
    @title = "Add bookmark"

    #@bookmark = Bookmark.new(params[:bookmark].merge({ :user_id => current_user.id }))
    @bookmark = Bookmark.new({ :title => params[:title],
                               :link => params[:link],
                               :user_id => current_user.id })

    if @bookmark.save
      render :text => @bookmark.id.to_s
    else
      @errors = @bookmark.errors.full_messages
      render :text => "-1"
    end

  end

  def destroy
    @title = "Delete bookmark"

    b = Bookmark.find(params[:id])
    if current_user.id == b.user_id
      Bookmark.destroy(b.id)
      @message = "Bookmark successfully deleted."
    else
      @message = "You do not have the necessary permissions to delete this bookmark."
    end
  end

  def edit
    @title = "Edit bookmark"
  end

  def export
    @bookmarks = Bookmark.all(:conditions => [ "user_id = ?", current_user.id ])
    if params[:type] == "xml"
      render :xml => @bookmarks
    elsif params[:type] == "json"
      render :json => @bookmarks
    else
      render :nothing => true
    end
  end
end
