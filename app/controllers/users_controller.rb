class UsersController < ApplicationController
  def new
    @title = "Sign up"
    @user = User.new
  end

  def create
    @title = "Sign up"

    @user = User.new(params[:user])

    if @user.save
      render "signup_success"
    else
      @user.password = ""
      @user.password_confirmation = ""
      @errors = @user.errors.full_messages
      render "new"
    end
  end
end
