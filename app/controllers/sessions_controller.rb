class SessionsController < ApplicationController
  def new
    @title = "Sign In"
    render "login"
  end

  def login
    @title = "Sign In"

    user = User.authenticate(params[:session][:email], params[:session][:password])
    if user.nil?
      @errors = ["Email and password do not match"]
      render "login"
    else
      user.reset_login_token!

      if params["remember_me"].to_i == 1
        cookies[:login_token] = { :value => user.login_token,
                                  :expires => 5.years.from_now.utc }
      else
        cookies[:login_token] = { :value => user.login_token }
      end 

      redirect_to home_path
    end
  end
 
  def logout
    cookies.delete(:login_token)
  end

end
