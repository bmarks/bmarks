class PagesController < ApplicationController
  def home
    @title = "Home"

    if logged_in?
      redirect_to bookmarks_path
    end
  end

  def about
    @title = "About"
  end

  def contact
    @title = "Contact Us"
  end

  def help
    @title = "Help"
  end

end
