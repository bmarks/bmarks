module SessionsHelper
  def current_user
    User.find_by_login_token(cookies[:login_token]) unless cookies[:login_token].nil?
  end

  def logged_in?
    not current_user.nil?
  end
end
