ActionController::Routing::Routes.draw do |map|
  #   map.connect 'products/:id', :controller => 'catalog', :action => 'view'

  # mapping static pages
  map.home    '/home',    :controller => "pages", :action => "home"
  map.about   '/about',   :controller => "pages", :action => "about"
  map.help    '/help',    :controller => "pages", :action => "help"
  map.contact '/contact', :controller => "pages", :action => "contact"

  map.signup  '/signup',  :controller => "users", :action => "new",
                          :conditions => { :method => :get }
  map.signup  '/signup',  :controller => "users", :action => "create",
                          :conditions => { :method => :post }

  map.login   '/login',   :controller => "sessions", :action => "new",
                          :conditions => { :method => :get }
  map.login   '/login',   :controller => "sessions", :action => "login",
                          :conditions => { :method => :post }
  map.logout  '/logout',  :controller => "sessions", :action => "logout"

  map.export  '/bookmarks/export/:type', :controller => "bookmarks", :action => "export"
  map.resources :bookmarks

  map.root    :controller => "pages", :action => "home"

  #map.connect ':controller/:action/:id'
  #map.connect ':controller/:action/:id.:format'
end
