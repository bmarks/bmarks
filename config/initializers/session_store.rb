# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_bmarks_session',
  :secret      => '14320dcfa8cc62d7363c2ac170ae906684296cec9e6dbf1847102bc293cf61a2f510635b99bdedfa9e8ffcfe8222aa8963d448ac95a95aba6d380a5e19782211'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
