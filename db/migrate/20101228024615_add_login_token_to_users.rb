class AddLoginTokenToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :login_token, :string
    add_index :users, :login_token
  end

  def self.down
    remove_column :users, :login_token
  end
end
