$(document).ready(function() {
  $("a.delete_bookmark").hide();
  $("a#add_bookmark").hide();
});

n = 0; // the id of the most recent "add bookmark" slot.  Resets automatically after page refresh

function insert_add_bookmark_field() {
  var newElement = '<div class="new_bookmark" id="new_bookmark_' + n + '">' +
                     '<a class="delete_bookmark" href="#" onclick="remove_add_bookmark_field(' + n + ')" id="new_delete_bookmark_' + n + '">' + 
                       '<img alt="Delete" class="delete_image" id="new_delete_image_' + n + '" src="/images/delete.png" /> ' + 
                         '<input type="text" size="25" value="Title" id="new_title_' + n + '" /> ' + 
                         '<input type="text" size="60" value="http://" id="new_link_' + n + '" /> ' +
                         '<a class="link_button add_done_button" href="#" onclick="add_bookmark_proxy(' + n + '); return false;">Done</a>' +
                   '</div>';

  $(newElement).hide().appendTo("div#bookmarks").fadeIn();
  n++;
}

function remove_add_bookmark_field(id) {
  $("div#new_bookmark_" + id).hide("fast");
}

function add_bookmark_proxy(id) {
  var title = $("#new_title_" + id).val();
  var link = $("#new_link_" + id).val();

  add_bookmark(title, link, id);
}

function add_bookmark(title, link, id) {
  $.ajax({
    type: "POST",
    data: {title: title, link: link},
    url: "/bookmarks",
    success: function(data, textStatus, XMLHttpRequest) {
      // data is the id of the bookmark
      var bmark_id = data;

      $("div#new_bookmark_" + id).hide("fast");
      // add the bookmark on the spot!
      setTimeout(function() {
        var newElement = '<div class="bookmark" id="bookmark_' + id + '">' +
                           '<a class="delete_bookmark" href="#" id="delete_bookmark_' + id + '" onclick="delete_bookmark(' + id + '); return false;">' +
                             '<img alt="Delete" class="delete_image" id="delete_image_' + id + '" src="/images/delete.png" />' +
                           '</a>' +
                           '<a href="' + link + '" class="bookmark_link" id="bookmark_link_' + id + '">' + title + '</a><br />' +
                         '</div>';
        $(newElement).hide().appendTo("div#bookmarks").fadeIn();
      }, 500);

      // remove 'no bookmark' message if present
      $("p#no_bookmarks").remove();
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      alert("Ooooops!  Something went awry.  Please try again:\n" + XMLHttpRequest.responseText);
    }
  });
}

function edit_bookmarks() {
  if ($("a#edit_button").text() == "edit") {
    $("a#edit_button").text("done");
    $("a.delete_bookmark").show("fast");
    $("a#add_bookmark").show("fast");
  } else {
    $("a#edit_button").text("edit");
    $("a.delete_bookmark").hide("fast");
    $("a#add_bookmark").hide("fast");
    $("div.new_bookmark").hide("fast");
    setTimeout(function() {
      $("div.new_bookmark").remove();
    }, 500);
  }
}

function delete_bookmark(bmark_id) {
  $.ajax({
    type: "DELETE",
    url: "/bookmarks/" + bmark_id,
    success: function(data, textStatus) {
      // hide the bookmark with animation
      $("div#bookmark_" + bmark_id).slideUp("fast");

      // then remove the id so it doesn't show up again
      $("div#bookmark_" + bmark_id).attr("id", "");
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      alert("Oooooops! Something went awry, please try again.");
    }
  });
}
